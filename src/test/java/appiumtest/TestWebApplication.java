package appiumtest;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class TestWebApplication {

	AppiumDriver<MobileElement> driver;

	public void setup() throws Exception {

		DesiredCapabilities cap = new DesiredCapabilities();

		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(MobileCapabilityType.NO_RESET, "true");
		cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 120);
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.chrome");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.google.android.apps.chrome.Main");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
		cap.setCapability("adbExecTimeout", 10000);

		driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4725/wd/hub"), cap);
		Thread.sleep(5000);
		driver.get("https://theoreminc.net");
		
		driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
		driver.findElement(By.id("wp-megamenu-main_menu")).click();
	}

	public void click() throws InterruptedException {
		driver.findElement(By.xpath("//a[@class='wpmm_mobile_menu_btn']")).click();
		Thread.sleep(2000);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestWebApplication obj = new TestWebApplication();
		try {
			obj.setup();
			obj.click();
		} catch (Exception e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Exception catch");
	}

}
